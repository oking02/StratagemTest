##Stratagem Coding Test

An Angular 1.5 application with a small Golang back-end.

The betting data is not live and was copied mostly from this website - http://www.statto.com/football/odds/europe/champions-league/winner
The lay odds for betfair where taken from - https://www.betfair.com/exchange/plus/#/football/market/1.125295479

####Dependencies

- Angular 1.5.8
- Angular-chart 1.0.3
- Bootstrap 3.3.7 (CSS only)
- fraction.js - 0.3

Was built using Go 1.7