package main

import (
	_"fmt"
	"os"
	"log"
	"encoding/json"
	"net/http"
)

var teams []Team

func init()  {
	teams = getData()
}

func handler(w http.ResponseWriter, r *http.Request) {
	json.NewEncoder(w).Encode(teams)
}

func main() {
	http.Handle("/", http.FileServer(http.Dir("public")))
	http.HandleFunc("/api/teams", handler)
	http.ListenAndServe(":80", nil)
}

type Team struct {
	Name	 string 	`json:"team"`
	Odds 	[]Odd  		`json:"odds"`
}

type Odd struct {
	Bookmaker 	string 		`json:"bookmaker"`
	Back 		float64 	`json:"back"`
	Lay 		float64 	`json:"lay"`
}

func getData() []Team  {
	file, err := os.Open("data.json")
	defer file.Close()
	var teams []Team
	if err != nil {
		log.Fatal(err)
		return teams
	}
	jsonParser := json.NewDecoder(file)
	jsonParser.Decode(&teams)
	return teams;

}
