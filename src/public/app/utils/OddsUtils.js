/**
 * Created by oking on 25/09/2016.
 */
(function() {
    'use strict';

    angular
        .module('demoApp')
        .factory('OddsUtils', OddsUtils);

    /**
     * Create a object with a number of utility function.
     */
    function OddsUtils() {

        var factory = {};

        /**
         * Use to format a decimal odd to a fraction string.
         * @param decimal
         * @returns {string}
         */
        factory.decimalToFraction = function (decimal) {
            var fraction = new Fraction(decimal);

            return fraction.denominator + "/" + fraction.numerator;
        };

        /**
         * Don't have time to source historic odds so
         * will fake it with this function
         *
         * @param odds
         * @returns {Array}
         */
        factory.mockHistoricData = function (odds) {
            // If no odds return
            if (odds.length == 0) {
                return [];
            }

            // Calculate the average odd.
            var sum = odds.reduce(function(accumulator, odd) {
                return accumulator + odd.back;
            }, 0);
            var average = sum / odds.length;

            // Array of the mock historic data.
            var historicData = [];

            // plot a historic trend using a random amount
            // based on the average
            odds.forEach(function() {
                var random = Math.random();
                var difference = random * average;
                historicData.push(average + difference)
            });

            return historicData;
        };

        return factory;

    }

}());