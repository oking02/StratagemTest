/**
 * Created by oking on 25/09/2016.
 */
(function() {

    angular
        .module('demoApp')
        .controller('InfoAsideController', ['$rootScope', 'leagueDataFactory', 'OddsUtils', InfoAsideController]);

    /**
     * Controller for the extra information aside
     *
     */
    function InfoAsideController($rootScope, leagueDataFactory, OddsUtils) {
        var vm = this;
        // Local copy to use when betType changes.
        var teamOdds = [];

        vm.betType = $rootScope.betType;
        vm.selectedTeam = {team: ''};

        // For mini table
        vm.teamOdds = [];

        // For price trend chart
        vm.chartData = [];

        vm.formatOdd = formatOdd;

        $rootScope.$watch('betType', function(type) {
            vm.betType = type;
            vm.teamOdds = teamOdds;
        });

        $rootScope.$watch('selectedTeam', function(team) {
            leagueDataFactory.getSingleTeam(team)
                .then(function(t) {
                    vm.selectedTeam = t;
                    vm.teamOdds = vm.selectedTeam.odds;
                    teamOdds = vm.selectedTeam.odds;
                    vm.chartData = OddsUtils.mockHistoricData(vm.teamOdds)
                })

        });

        // Arbitrary months for historic data.
        vm.labels = ["January", "February", "March"];
        vm.datasetOverride = [{ yAxisID: 'y-axis-1' }];
        vm.options = {
            scales: {
                yAxes: [
                    {
                        id: 'y-axis-1',
                        type: 'linear',
                        display: true,
                        position: 'left'
                    }
                ]
            }
        };

        function formatOdd (odd) {
            if ($rootScope.betType == 'decimal') {
                return odd;
            } else {
                return OddsUtils.decimalToFraction(odd);
            }
        }

    }

}());