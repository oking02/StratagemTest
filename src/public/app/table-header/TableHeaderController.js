/**
 * Created by oking on 25/09/2016.
 */

(function() {
    'use strict';

    angular
        .module('demoApp')
        .controller('TableHeaderController', ['$rootScope', TableHeaderController]);

    /**
     * Controller for the main table view.
     * Mainly used to control the toggling of bet types
     */
    function TableHeaderController($rootScope) {
        var vm = this;

        vm.betType = $rootScope.betType;

        vm.changeBetType = changeBetType;

        function changeBetType (type) {
            $rootScope.betType = type;
        }

        $rootScope.$watch('betType', function(type) {
            vm.betType = type;
        })
    }

}());