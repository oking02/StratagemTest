/**
 * Created by oking on 24/09/2016.
 */

(function(){

    var app = angular.module('demoApp', ['chart.js']);

    app.run(function($rootScope) {

        $rootScope.betType = 'decimal';
        $rootScope.selectedTeam = '';

    })

}());