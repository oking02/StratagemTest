/**
 * Created by oking on 25/09/2016.
 */
(function() {
    'use strict';

    angular
        .module('demoApp')
        .factory('leagueDataFactory',['$http', '$q', LeagueDataFactory]);

    function LeagueDataFactory($http, $q) {

        // Cached version of teamlist
        var teamList;

        var factory = {};

        factory.getLeagueTeams = function () {
            // If cache version available use that otherwise do http GET
            return $q.when(teamList || this.fetchTeams());
        };

        factory.fetchTeams = function () {
            var defer = $q.defer();
            $http({
                method: 'GET',
                url: '/api/teams'
            }).then(function (response) {
                teamList = response.data;
                defer.resolve(teamList)
            });

            return defer.promise;
        };

        factory.getSingleTeam = function (team) {
            var defer = $q.defer();

            this.getLeagueTeams()
                .then(function(teams) {
                    teams = teams.filter(function(t) {
                        return t.team == team
                    });

                    defer.resolve(teams[0])
                });

            return defer.promise;
        };

        return factory;

    }

}());