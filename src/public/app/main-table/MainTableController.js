/**
 * Created by oking on 24/09/2016.
 */

(function() {
    'use strict';

    angular
        .module('demoApp')
        .controller('MainTableController', ['$rootScope', 'leagueDataFactory', 'OddsUtils',  MainTableController]);

    /**
     * Controller for the main table of teams and there best odds.
     */
    function MainTableController($rootScope, leagueDataFactory, OddsUtils) {
        var vm = this;

        // local copy to use when switching bet format
        var teamData = [];

        vm.tableData = [];

        vm.sortType = 'team';
        vm.sortReverse = false;

        vm.betType = $rootScope.betType;
        vm.selectedTeam = $rootScope.selectedTeam;

        $rootScope.$watch('betType', function(type) {
            vm.betType = type;
            vm.tableData = teamData;
        });

        $rootScope.$watch('selectedTeam', function(team) {
            vm.selectedTeam = team;
        });

        vm.changeFilter = changeFilter;
        vm.selectTeam = selectTeam;
        vm.formatOdd = formatOdd;

        init();

        // Make initial call to the back-end.
        // Map over collection and find best value bet for each
        function init () {

            leagueDataFactory.getLeagueTeams()
                .then(function(data) {
                    var teams = data;

                    teams = teams.map(function(team) {

                        var odds = team.odds;
                        odds.sort(function(a,b) {
                            return b.back - a.back;
                        });

                        var best = odds[0];

                        var t = team;
                        t.bookmaker = best.bookmaker;
                        t.back = best.back;
                        t.lay = best.lay;

                        return t;

                    });

                    teamData = teams;
                    vm.tableData = teams;
                    $rootScope.selectedTeam = teams[0].team;
                })

        }

        function changeFilter (filter) {

            if (filter == vm.sortType) {
                vm.sortReverse = !vm.sortReverse;
            } else {
                vm.sortReverse = false;
                vm.sortType = filter;
            }

        }

        function selectTeam (team) {
            $rootScope.selectedTeam = team;
        }

        function formatOdd (odd) {
            if ($rootScope.betType == 'decimal') {
                return odd;
            } else {
                return OddsUtils.decimalToFraction(odd);
            }
        }
    }

}());